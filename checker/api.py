import string
import sys
import tika
from tika import parser
import requests
from random import randint, choice
from user_agent import generate_user_agent

STATUS_UP = 'UP'
STATUS_DOWN = 'DOWN'
STATUS_CORRUPT = 'CORRUPT'
STATUS_MUMBLE = 'MUMBLE'

timeout = 30

ua = generate_user_agent()


class CheckException(Exception):
    def __init__(self, status, description, error):
        self.status = status
        self.description = description
        self.error = error


def random_string(length: int):
    chars = string.ascii_letters + string.digits
    return ''.join(choice(chars) for _ in range(length))


def do_register(session: requests.Session, url: str, secret=random_string(randint(15, 40))):
    username = random_string(randint(4, 20))
    password = random_string(randint(12, 40))
    try:
        r = session.post(url + '/register',
                         timeout=timeout,
                         headers={"User-Agent": ua},
                         data={"username": username, "password": password, "password2": password, "secret": secret})
    except Exception as e:
        raise CheckException(STATUS_DOWN, 'Can\'t send register request', str(e))
    if r.status_code == 404:
        raise CheckException(STATUS_DOWN, 'Can\'t send register request', "404 on register")
    if r.status_code != 200:
        raise CheckException(STATUS_MUMBLE, f'/register returned code {r.status_code}', r.text)
    return username, password, secret


def do_login(session: requests.Session, url: str, username: str, password: str):
    try:
        r = session.post(url + '/login',
                         timeout=timeout,
                         headers={"User-Agent": ua},
                         data={"username": username, "password": password})
        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/login returned code {r.status_code}', r.text)
        r = session.get(url + '/friends',
                        timeout=timeout,
                        headers={"User-Agent": ua}
                        )
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send login request', str(e))
    if r.url != f"{url}/friends":
        raise CheckException(STATUS_MUMBLE, f'Invalid Credentials', r.text)


def get_recovery_code(session: requests.Session, url: str) -> str:
    try:
        r = session.get(url + '/recovery_code',
                        timeout=timeout,
                        headers={"User-Agent": ua}
                        )
        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/recovery_code returned code {r.status_code}', r.text)
        return r.json()['recovery_code']
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t get recovery code', str(e))


def reset_password(session: requests.Session, url: str, username: str, recovery_code: str):
    password = random_string(randint(12, 40))
    try:
        r = session.post(url + '/recovery',
                         timeout=timeout,
                         headers={"User-Agent": ua},
                         data={"username": username,
                               "recovery_code": recovery_code,
                               "password": password,
                               "password2": password
                               })
        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/recovery returned code {r.status_code}', r.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t reset password', str(e))
    try:
        do_login(session, url, username, password)
    except CheckException as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t use new password', str(e))
    return password


def show_users(session: requests.Session, url: str, username: str):
    try:
        r = session.get(url + '/users',
                        timeout=timeout,
                        headers={"User-Agent": ua}
                        )
        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/users returned code {r.status_code}', r.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t get user list', str(e))
    if username not in r.text:
        raise CheckException(STATUS_MUMBLE, 'Can\'t find myself in user list', r.text)


def send_friend_request(friend1: requests.Session, friend2: requests.Session, url: str, username1: str, username2: str):
    try:
        r1 = friend1.post(url + '/friends',
                          timeout=timeout,
                          headers={"User-Agent": ua},
                          data={"username": username2}
                          )
        r2 = friend2.get(url + '/friends',
                         timeout=timeout,
                         headers={"User-Agent": ua},
                         )
        if r1.status_code != 200 and r2.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/friends returned code F1:{r1.status_code} F2:{r2.status_code}',
                                 r1.text + '\n' + r2.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send friend request', str(e))
    if username1 not in r2.text:
        raise CheckException(STATUS_MUMBLE, 'Can\'t find friend request', r1.text + '\n' + r2.text)
    try:
        r2 = friend2.post(url + '/friends',
                          timeout=timeout,
                          headers={"User-Agent": ua},
                          data={"username": username1}
                          )
        r1 = friend1.get(url + '/friends',
                         timeout=timeout,
                         headers={"User-Agent": ua},
                         )
        if r1.status_code != 200 and r2.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/friends returned code F1:{r1.status_code} F2:{r2.status_code}',
                                 r1.text + '\n' + r2.text)
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send accept friendship', str(e))
    if username2 not in r1.text:
        raise CheckException(STATUS_MUMBLE, 'Can\'t find friend', r1.text + '\n' + r2.text)


def get_friend_secret(session: requests.Session, url: str, username: str):
    try:
        r = session.get(url + '/ticket?friend=' + username,
                        timeout=timeout,
                        headers={"User-Agent": ua},
                        stream=True)
        if r.status_code != 200:
            raise CheckException(STATUS_MUMBLE, f'/ticket returned code {r.status_code}', str(r.text))
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t get friend\'s ticket', str(e))
    try:
        ticket_secret = parser.from_buffer(r.raw)
        return ticket_secret['content'].replace('\n', '')
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t read the ticket', str(e))


def check_functionality(ip: str):
    try:
        auth_checker = requests.Session()
        url = "http://" + ip
        username, password, secret = do_register(auth_checker, url)
        auth_checker.cookies.clear()
        do_login(auth_checker, url, username, password)
        recovery_code = get_recovery_code(auth_checker, url)
        auth_checker.cookies.clear()
        password = reset_password(auth_checker, url, username, recovery_code)
        do_login(auth_checker, url, username, password)
        show_users(auth_checker, url, username)
        friend1 = requests.Session()
        friend2 = requests.Session()
        username1, password1, secret1 = do_register(friend1, url)
        username2, password2, secret2 = do_register(friend2, url)
        send_friend_request(friend1, friend2, url, username1, username2)
        get_friend_secret(friend1, url, username2)

        print(STATUS_UP)
        print('Everything is fine')
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print('MUMBLE')
        print('Error during check, contact admins')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)


def push_flag(ip, flag):
    try:
        url = "http://" + ip
        flag_getter = requests.session()
        flag_carrier = requests.session()
        username1, password1, secret1 = do_register(flag_getter, url)
        username2, password2, secret2 = do_register(flag_carrier, url, flag)
        send_friend_request(flag_getter, flag_carrier, url, username1, username2)

        print(STATUS_UP)
        print('Everything is fine')

        return username1, password1, username2
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print('MUMBLE')
        print('Error during check, contact admins')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)


def pull_flag(ip, flag, username: str, password: str, flag_carrier: str):
    try:
        url = "http://" + ip
        flag_getter = requests.session()
        do_login(flag_getter, url, username, password)
        c_flag = get_friend_secret(flag_getter, url, flag_carrier)
        if flag in c_flag:
            print(STATUS_UP)
            print('Everything is fine')
        else:
            print(STATUS_CORRUPT)
            print('Flag not found')
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print('MUMBLE')
        print('Error during flag pull, could be changed html selectors')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)
