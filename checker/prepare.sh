#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pip3 install wheel
cd $DIR
mv tika-server.jar /tmp/
mv tika-server.jar.md5 /tmp/
pip3 install --upgrade pip
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
