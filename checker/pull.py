from api import pull_flag
import sys
from redis import StrictRedis

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
hostname = sys.argv[3]
oldFlag = sys.argv[4]

redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)

username = redis.get(f'checkers_state/friendship/{hostname}/username')
password = redis.get(f'checkers_state/friendship/{hostname}/password')
flag_carrier = redis.get(f'checkers_state/friendship/{hostname}/flag_carrier')
pull_flag(hostname, oldFlag, username, password, flag_carrier)
