from api import push_flag
import sys
from redis import StrictRedis

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
hostname = sys.argv[3]
newFlag = sys.argv[4]

username, password, flag_carrier = push_flag(hostname, newFlag)

if username:
    redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)
    redis.set(f'checkers_state/friendship/{hostname}/username', username)
    redis.set(f'checkers_state/friendship/{hostname}/password', password)
    redis.set(f'checkers_state/friendship/{hostname}/flag_carrier', flag_carrier)
