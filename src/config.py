import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = "Don't change me!"
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or 'sqlite:///app.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    OPTIONS = {
        'page-height': '2.1in',
        'page-width': '2.7in',
        'encoding': "UTF-8",
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
        "enable-local-file-access": True,
        'no-outline': None
    }
