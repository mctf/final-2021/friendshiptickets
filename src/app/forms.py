from flask_login import current_user
from flask_wtf import FlaskForm
from app.models import User, Requests
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, EqualTo, Length, ValidationError


class FriendshipForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    submit = SubmitField("Send friend request")

    def validate_username(self, username):
        friend = User.query.filter_by(username=username.data).first()
        if friend is None or Requests.query.filter_by(initiator=current_user.id, friend=friend.id).first() is not None \
                or friend.id == current_user.id:
            raise ValidationError("Bad friend request")


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Sign in")


class RegistrationForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired(), Length(min=12)])
    password2 = PasswordField("Repeat Password", validators=[DataRequired(), EqualTo("password")])
    secret = StringField("Tell me your secret!", validators=[DataRequired()])
    submit = SubmitField("Register")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Please use a different username.")


class ResetPasswordForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    recovery_code = StringField("Recovery Code", validators=[DataRequired(), Length(max=3)])
    password = PasswordField("Password", validators=[DataRequired(), Length(min=12)])
    password2 = PasswordField("Repeat Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Reset Password")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is None:
            raise ValidationError("Username not found.")

