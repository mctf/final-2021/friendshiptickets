import time

from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class Friendship(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    friend1 = db.Column('friend1', db.Integer, db.ForeignKey("user.id"))
    friend2 = db.Column('friend2', db.Integer, db.ForeignKey("user.id"))


class Requests(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    initiator = db.Column("initiator", db.Integer, db.ForeignKey("user.id"))
    friend = db.Column("friend", db.Integer, db.ForeignKey("user.id"))

    def accept_friendship(self):
        if self.initiator > self.friend:
            friendship = Friendship(friend1=self.initiator, friend2=self.friend)
        else:
            friendship = Friendship(friend1=self.friend, friend2=self.initiator)
        db.session.add(friendship)
        db.session.delete(Requests.query.filter_by(initiator=self.friend, friend=self.initiator).first())
        db.session.commit()



class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(1028), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    recovery_code = db.Column(db.String(128))
    time = db.Column(db.Integer())
    secret = db.Column(db.String(128))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password, "plain")

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def verify_reset_password(username, recovery_code):
        return User.query.filter_by(username=username, recovery_code=recovery_code) is not None



@login.user_loader
def load_user(id):
    return User.query.get(int(id))
