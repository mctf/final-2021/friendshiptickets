import random
import time
import pdfkit
from app import app, db
from app.forms import LoginForm, RegistrationForm, FriendshipForm, ResetPasswordForm
from app.models import User, Friendship, Requests
from flask import url_for, redirect, flash, render_template, make_response, request
from flask_login import current_user, login_user, logout_user, login_required
from app import Config


@app.route('/')
def index():
    if current_user.is_authenticated:
        return render_template("index.html", title="Index", username=current_user.username)
    return render_template("index.html", title="Index", username="stranger")


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm(meta={'csrf': False})
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for("index"))
    return render_template("login.html", title="Login", form=form)


@app.route('/register', methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    form = RegistrationForm(meta={'csrf': False})
    if form.validate_on_submit():
        user = User(username=form.username.data, recovery_code=str(random.randint(0, 999)), secret=form.secret.data,
                    time=time.time())
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        flash("Congrats! You are now a registered user!")
        return redirect(url_for("index"))
    return render_template("register.html", title="Register", form=form)


@app.route('/friends', methods=["GET", "POST"])
@login_required
def friends():
    form = FriendshipForm(meta={'csrf': False})
    if form.validate_on_submit():
        friend = User.query.filter_by(username=form.username.data).first()
        request = Requests(initiator=current_user.id, friend=friend.id)
        if Friendship.query.filter(((Friendship.friend1 == current_user.id) & (Friendship.friend2 == friend.id)) |
                                   ((Friendship.friend2 == current_user.id) & (Friendship.friend1 == friend.id))) \
                .first() is not None:
            flash("You can't send request to your friend!")
            return redirect(url_for('friends'))
        if Requests.query.filter_by(initiator=friend.id, friend=current_user.id).first() is not None:
            request.accept_friendship()
            flash(f"You and {friend.username} are friends now!")
            return redirect(url_for('friends'))
        flash(f"Friend request sent to {friend.username}!")
        db.session.add(request)
        db.session.commit()
        return redirect(url_for('friends'))
    friends_requests = User.query.filter(User.id.in_(
        Requests.query.with_entities(Requests.initiator).filter_by(friend=current_user.id).subquery())).all()
    friends = User.query.filter(User.id.in_(Friendship.query.with_entities(Friendship.friend2)
                                            .filter_by(friend1=current_user.id).subquery())) \
        .union(User.query.filter(User.id.in_(Friendship.query.with_entities(Friendship.friend1)
                                             .filter_by(friend2=current_user.id)
                                             .subquery()))).all()
    return render_template("friends.html", title="Friends", form=form, friends_request=friends_requests,
                           friends=friends)


@app.route('/users')
def users():
    return render_template('users.html', title='Users', users=User.query.filter(User.time >= time.time() - 600).all())


@app.route('/recovery', methods=["GET", "POST"])
def recovery():
    form = ResetPasswordForm(meta={'csrf': False})
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data, recovery_code=form.recovery_code.data).first()
        if user is None:
            flash("Wrong Recovery Code!")
            return redirect(url_for('recovery'))
        user.set_password(form.password.data)
        db.session.commit()
        flash("Password changed!")
        logout_user()
        return redirect(url_for('login'))
    return render_template('recovery.html', title='Recovery', form=form)


@app.route('/recovery_code', methods=["GET"])
@login_required
def recovery_code():
    return {"recovery_code": current_user.recovery_code}


@app.route('/ticket', methods=["GET"])
@login_required
def generate_ticket():
    username = request.args.get('friend', type=str)
    if not username:
        return render_template('tickets.html')
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash("Can't find user!")
        return redirect(url_for('index'))
    friendship = Friendship.query.filter(((Friendship.friend1 == user.id) | (Friendship.friend2 == current_user.id))
                                         | ((Friendship.friend2 == user.id)
                                            & (Friendship.friend1 == current_user.id))).first()
    if friendship is None:
        flash("You're not friends!")
        return redirect(url_for('index'))
    template = f'''
    <!doctype html>
    <html>
    <head>
    <style type="text/css">
  body{{
    font-family:Arvo;
  }}
  .box{{
    margin:0px auto;
    margin-top:80px;
    color:#333;
    text-transform:uppercase;
    padding:8px;
    width:300px;
    font-weight:bold;
    text-shadow:0px 1px 0px #fff;
    font-family:"arvo";
    font-size: 11px;
    border-left: 1px dashed rgba(51, 51, 51, 0.5);
    -webkit-filter: drop-shadow(0 5px 18px #000);
  }}

  .inner{{
    border: 2px dashed rgba(51, 51, 51, 0.5);
    min-height:100px;
    padding:8px;
    
  }}
  .inner h1{{
    padding:5px 0px;
    margin:0px;
    font-size:18px;
    border-bottom: 1px solid rgba(51, 51, 51, 0.3);
    text-align:center;
  }}
  .info{{
    width:100%;
    margin-top:5px;
  }}
  .info .wp{{
    float: left;
    padding: 5px;
    width: 83px;
    text-align: center;
  }}
  .info .wp h2{{
    margin: 8px;
  }}
  .total{{
    border-top: 1px solid rgba(51, 51, 51, 0.3);
  }}
  
  .clearfix:after {{
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
  }}
  .total h2{{
    padding-left:5px;
    margin:10px 0px; 
    font-size: 15px;
  }}
  .total p{{float:right;margin:0px;margin-right: 15px;}}
  
  a.buy{{
    color:#fff;
    text-align:center;
    font-size:25px;
    text-decoration:none;
    display:block;
    width:200px;
    margin:0px auto;
    margin-top:50px;
    padding:5px;
    background:rgba(0,0,0,0.5);
  }}
    </style>
    <title>Friendship Ticket!</title>
    </head>
    <body>
      <div class="box">
    <div class='inner'>
    <h1>Ticket to Friendship!</h1>
    <div class='info clearfix'>
      <div class='wp'>Tickets<h2>1</h2></div>
      <div class='wp'>Type<h2>Friend</h2></div>
      <div class='wp'>Affection<h2>100%</h2></div>
    </div>
    <div class='total clearfix'>
      <h2>{user.secret}</h2>
    </div>
    </div>
  </div>
    </body>
    </html>
    '''
    response = make_response(pdfkit.from_string(template, options=Config.OPTIONS))
    response.headers['Content-Type'] = 'application/pdf'
    return response
