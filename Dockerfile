FROM ubuntu:20.04
WORKDIR /app
ENV PYTHONUNBUFFERED 1
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
	nginx=1.18.* \
	python3-pip=20.0.* \
	python3-wheel=0.34.* \
	python3-setuptools=45.2.* \
	python3-dev=3.8.* \
	supervisor=4.1.* \
	netcat-openbsd=1.* \
	curl=7.68.* \
    python3-psycopg2 \
    wget \
    fontconfig libxext6 libxrender1 xfonts-75dpi xfonts-base \
	&& rm -rf /var/lib/apt/lists/*

COPY ./docker/wkhtmltox_0.12.6-1.focal_amd64.deb /tmp/wkhtmltox_0.12.6-1.focal_amd64.deb
RUN apt install -f /tmp/wkhtmltox_0.12.6-1.focal_amd64.deb
RUN rm /tmp/wkhtmltox_0.12.6-1.focal_amd64.deb
COPY ./docker/nginx-global.conf /etc/nginx/nginx.conf
COPY ./docker/nginx-site.conf /etc/nginx/sites-available/default

COPY ./docker/supervisord.conf /etc/supervisord.conf

COPY src/requirements.txt ./
RUN pip3 --disable-pip-version-check --no-cache-dir \
	install -r ./requirements.txt

COPY src /app/
RUN chown -R www-data:www-data /app && \
	chown -R www-data:www-data /var/lib/nginx
USER 33
ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
